# British Library PID case study for FREYA project

- Open live notebook: [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/bl-data-services%2Fbl-freya-pid-case-study/main?filepath=british-library-dois.ipynb)
- Open rendered version (hides the code): [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/bl-data-services%2Fbl-freya-pid-case-study/main?urlpath=%2Fvoila%2Frender%2Fbritish-library-dois.ipynb)

As part of the [FREYA project](https://project-freya.eu/en) a case study on the use of the PID graph in the context of The British Library collections and research outputs is being developed here.

## Useful info

### British Library identifiers

- ISNI: [0000 0001 2308 1542](http://isni.org/isni/0000000123081542)
- ROR: <https://ror.org/05dhe8b71>
- Grid: [grid.36212.34](https://grid.ac/institutes/grid.36212.34)
