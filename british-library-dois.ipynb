{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Plot information about British Library DOIs from various sources\n",
    "\n",
    "The British Library is a founding partner in [DataCite](https://datacite.org/), and now leads the main UK DataCite consortium of higher education and other non-profit research organisations. As well as providing DataCite DOI services to other organisations, we also use them internally in a number of contexts, including [our research repository](https://bl.oar.bl.uk/) and [our metadata datasets](https://www.bl.uk/collection-metadata/downloads).\n",
    "\n",
    "This notebook shows how to use the DataCite GraphQL API to obtain information about the British Library's DOIs, [display a summary table](#Display-statistics-about-the-works) and [plot a graph of new DOIs over time](#Plot-new-DOIs-over-time) using some common Python-based tools.\n",
    "\n",
    "If you are viewing this notebook live in Jupyter, you may need to select **Restart Kernel and Run All Cells…** from the **Kernel** menu to re-run the query and make the graph display correctly."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Install libraries and prepare GraphQL client"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%capture\n",
    "# Uncomment below and run this cell to install required Python packages\n",
    "# NB. This should already be installed if you're running on mybinder.org\n",
    "\n",
    "#!pip install pipenv\n",
    "#!pipenv install --system"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Prepare the GraphQL client\n",
    "import requests\n",
    "from IPython.display import display, Markdown\n",
    "from gql import gql, Client\n",
    "from gql.transport.requests import RequestsHTTPTransport\n",
    "\n",
    "_transport = RequestsHTTPTransport(\n",
    "    url='https://api.datacite.org/graphql',\n",
    "    use_json=True,\n",
    ")\n",
    "\n",
    "client = Client(\n",
    "    transport=_transport,\n",
    "    fetch_schema_from_transport=True,\n",
    ")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Prepare templating library to make code cleaner\n",
    "%load_ext template_magic\n",
    "from mako.template import Template"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Define and run the GraphQL query\n",
    "\n",
    "First we define the GraphQL query to find all works from the British Library's repositories, using the catch-all query `*`. For flexibility, the parameters for the query are kept in the `query_params` variable:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Generate the GraphQL query\n",
    "query_params = {\n",
    "    \"oar_repository\":      \"bl.repo\",\n",
    "    \"mrd_repository\":      \"bl.jisc-mrd\",\n",
    "    \"ukwa_repository\":     \"bl.wap\",\n",
    "    \"metadata_repository\": \"bl.metadata\",\n",
    "    \"eap_repository\":      \"bl.eap\",\n",
    "    \"old_repository\":      \"bl.old-api\",\n",
    "    \"labs_repository\":     \"bl.labs\",\n",
    "\n",
    "    \"keyword\":             \"*\",\n",
    "}\n",
    "repo_names = [param[:-11] for param in query_params.keys()\n",
    "               if param.endswith(\"_repository\")]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we use this information to fill in a template. The lines that begin `% for repo in ...` and `% endfor` repeat that part of the template once for each repository; this enables us to keep our code simple and avoid errors that might creep in repeating the same subquery for each repository. If you want to learn more, [take a look at the Mako templating language website](https://www.makotemplates.org/)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [],
   "source": [
    "query_template = Template(\"\"\"query getWorksByRepositoryAndKeyword(\n",
    "% for repo in repo_names:\n",
    "    $${repo}_repository: ID!,\n",
    "% endfor\n",
    "    $keyword: String!\n",
    "    )\n",
    "{\n",
    "% for repo in repo_names:\n",
    "  ${repo}: repository(id: $${repo}_repository) {\n",
    "    id\n",
    "    name\n",
    "    citationCount\n",
    "    viewCount\n",
    "    downloadCount\n",
    "    works(query: $keyword) {\n",
    "      totalCount\n",
    "      published {\n",
    "        title\n",
    "        count\n",
    "      }\n",
    "      nodes {\n",
    "        id\n",
    "        type\n",
    "        publicationYear\n",
    "        bibtex\n",
    "        titles {\n",
    "          title\n",
    "        }\n",
    "        citationCount\n",
    "        viewCount\n",
    "        downloadCount\n",
    "      }\n",
    "    }\n",
    "  },\n",
    "% endfor\n",
    "}\n",
    "\"\"\")\n",
    "query = gql(query_template.render(repo_names=repo_names))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Finally, we execute the query using the GraphQL client. Running this query may take quite a while (around 10-15 seconds)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [],
   "source": [
    "import json\n",
    "data = client.execute(query, variable_values=json.dumps(query_params))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Display statistics about the works\n",
    "\n",
    "If the above cells executed without error, we now have information about the DOIs for each repository stored in the variable `data`, so now we can examine and summarise it.\n",
    "\n",
    "The cell below is another Mako template, which formats a summary table in Markdown format, showing for each repository the total number of works, citations, downloads and views."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/markdown": [
       "\n",
       "Query: `*`\n",
       "\n",
       "| Repository name | Works | Citations | Downloads | Views |\n",
       "|-----------------|-------|-----------|-----------|-------|\n",
       "| British Library Repository | 669 | 0 | 0 | 0 |\n",
       "| BL test account | 0 | 0 | 0 | 0 |\n",
       "| Web Archive Programme at BL | 6 | 0 | 0 | 0 |\n",
       "| British Library Metadata Standards | 277 | 0 | 0 | 0 |\n",
       "| Endangered Archives Project | 364 | 0 | 0 | 0 |\n",
       "| BL's DOI service prototype | 0 | 0 | 0 | 0 |\n",
       "| British Library Labs | 105 | 0 | 0 | 0 |\n"
      ],
      "text/plain": [
       "<IPython.core.display.Markdown object>"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "%%mako\n",
    "\n",
    "Query: `${query_params['keyword']}`\n",
    "\n",
    "| Repository name | Works | Citations | Downloads | Views |\n",
    "|-----------------|-------|-----------|-----------|-------|\n",
    "% for repo in repo_names:\n",
    "| ${data[repo]['name']} | ${data[repo]['works']['totalCount']} | ${data[repo]['citationCount']} | ${data[repo]['downloadCount']} | ${data[repo]['viewCount']} |\n",
    "% endfor"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Unfortunately, this isn't as interesting as we might like, since the figures for citations, downloads and views are all zero. This is not because there is no usage, but because what usage there is isn't currently submitted to the DataCite database.\n",
    "\n",
    "### Plot new DOIs over time\n",
    "\n",
    "Now we can plot the number of works per year split across the repositories."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "First, we convert the hierarchical data structure returned from the GraphSQL query into a table with one row per repository per year, containing the number of DOIs published in that year. We import the popular [pandas data analysis & manipulation tool for Python](https://pandas.pydata.org/), and create a `DataFrame` (pandas' standard object for tabular data) by combining tables for each separate repository (using `pd.concat`):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [],
   "source": [
    "import pandas as pd\n",
    "from IPython.display import display\n",
    "\n",
    "# Make a list of DataFrames, one for each repository\n",
    "per_repository_counts = [pd.DataFrame(data[repo]['works']['published']) for repo in repo_names]\n",
    "\n",
    "df = (pd.concat(per_repository_counts, keys=repo_names)    # Join repository data into a single table\n",
    "    .reset_index().drop(columns='level_1')                 # Remove the unnecessary \"level_1\" column\n",
    "    .rename(columns={'title': 'year', 'level_0': 'repo'})) # Rename columns to something more intuitive\n",
    "\n",
    "# Add a new column by extracting the name for each repository\n",
    "df['repo_title'] = df['repo'].map(lambda repo: data[repo]['name'])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's look at a truncated version to see what this structure looks like:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "<div>\n",
       "<style scoped>\n",
       "    .dataframe tbody tr th:only-of-type {\n",
       "        vertical-align: middle;\n",
       "    }\n",
       "\n",
       "    .dataframe tbody tr th {\n",
       "        vertical-align: top;\n",
       "    }\n",
       "\n",
       "    .dataframe thead th {\n",
       "        text-align: right;\n",
       "    }\n",
       "</style>\n",
       "<table border=\"1\" class=\"dataframe\">\n",
       "  <thead>\n",
       "    <tr style=\"text-align: right;\">\n",
       "      <th></th>\n",
       "      <th>repo</th>\n",
       "      <th>year</th>\n",
       "      <th>count</th>\n",
       "      <th>repo_title</th>\n",
       "    </tr>\n",
       "  </thead>\n",
       "  <tbody>\n",
       "    <tr>\n",
       "      <th>0</th>\n",
       "      <td>oar</td>\n",
       "      <td>2020</td>\n",
       "      <td>37.0</td>\n",
       "      <td>British Library Repository</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>1</th>\n",
       "      <td>oar</td>\n",
       "      <td>2019</td>\n",
       "      <td>27.0</td>\n",
       "      <td>British Library Repository</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>2</th>\n",
       "      <td>oar</td>\n",
       "      <td>2018</td>\n",
       "      <td>24.0</td>\n",
       "      <td>British Library Repository</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>3</th>\n",
       "      <td>oar</td>\n",
       "      <td>2017</td>\n",
       "      <td>11.0</td>\n",
       "      <td>British Library Repository</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>...</th>\n",
       "      <td>...</td>\n",
       "      <td>...</td>\n",
       "      <td>...</td>\n",
       "      <td>...</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>28</th>\n",
       "      <td>labs</td>\n",
       "      <td>2018</td>\n",
       "      <td>16.0</td>\n",
       "      <td>British Library Labs</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>29</th>\n",
       "      <td>labs</td>\n",
       "      <td>2017</td>\n",
       "      <td>40.0</td>\n",
       "      <td>British Library Labs</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>30</th>\n",
       "      <td>labs</td>\n",
       "      <td>2016</td>\n",
       "      <td>48.0</td>\n",
       "      <td>British Library Labs</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>31</th>\n",
       "      <td>labs</td>\n",
       "      <td>2013</td>\n",
       "      <td>1.0</td>\n",
       "      <td>British Library Labs</td>\n",
       "    </tr>\n",
       "  </tbody>\n",
       "</table>\n",
       "<p>32 rows × 4 columns</p>\n",
       "</div>"
      ],
      "text/plain": [
       "    repo  year  count                  repo_title\n",
       "0    oar  2020   37.0  British Library Repository\n",
       "1    oar  2019   27.0  British Library Repository\n",
       "2    oar  2018   24.0  British Library Repository\n",
       "3    oar  2017   11.0  British Library Repository\n",
       "..   ...   ...    ...                         ...\n",
       "28  labs  2018   16.0        British Library Labs\n",
       "29  labs  2017   40.0        British Library Labs\n",
       "30  labs  2016   48.0        British Library Labs\n",
       "31  labs  2013    1.0        British Library Labs\n",
       "\n",
       "[32 rows x 4 columns]"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "pd.options.display.max_rows = 8\n",
    "display(df)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we use the [Altair visualisation library](https://altair-viz.github.io/) to plot this in a chart, using colors to separate out the repositories. the call to `Chart.encode` shows how the columns of `df` are mapped to elements of the chart."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "\n",
       "<div id=\"altair-viz-df3d257f8e92441fb741eeede629c333\"></div>\n",
       "<script type=\"text/javascript\">\n",
       "  (function(spec, embedOpt){\n",
       "    let outputDiv = document.currentScript.previousElementSibling;\n",
       "    if (outputDiv.id !== \"altair-viz-df3d257f8e92441fb741eeede629c333\") {\n",
       "      outputDiv = document.getElementById(\"altair-viz-df3d257f8e92441fb741eeede629c333\");\n",
       "    }\n",
       "    const paths = {\n",
       "      \"vega\": \"https://cdn.jsdelivr.net/npm//vega@5?noext\",\n",
       "      \"vega-lib\": \"https://cdn.jsdelivr.net/npm//vega-lib?noext\",\n",
       "      \"vega-lite\": \"https://cdn.jsdelivr.net/npm//vega-lite@4.8.1?noext\",\n",
       "      \"vega-embed\": \"https://cdn.jsdelivr.net/npm//vega-embed@6?noext\",\n",
       "    };\n",
       "\n",
       "    function loadScript(lib) {\n",
       "      return new Promise(function(resolve, reject) {\n",
       "        var s = document.createElement('script');\n",
       "        s.src = paths[lib];\n",
       "        s.async = true;\n",
       "        s.onload = () => resolve(paths[lib]);\n",
       "        s.onerror = () => reject(`Error loading script: ${paths[lib]}`);\n",
       "        document.getElementsByTagName(\"head\")[0].appendChild(s);\n",
       "      });\n",
       "    }\n",
       "\n",
       "    function showError(err) {\n",
       "      outputDiv.innerHTML = `<div class=\"error\" style=\"color:red;\">${err}</div>`;\n",
       "      throw err;\n",
       "    }\n",
       "\n",
       "    function displayChart(vegaEmbed) {\n",
       "      vegaEmbed(outputDiv, spec, embedOpt)\n",
       "        .catch(err => showError(`Javascript Error: ${err.message}<br>This usually means there's a typo in your chart specification. See the javascript console for the full traceback.`));\n",
       "    }\n",
       "\n",
       "    if(typeof define === \"function\" && define.amd) {\n",
       "      requirejs.config({paths});\n",
       "      require([\"vega-embed\"], displayChart, err => showError(`Error loading script: ${err.message}`));\n",
       "    } else if (typeof vegaEmbed === \"function\") {\n",
       "      displayChart(vegaEmbed);\n",
       "    } else {\n",
       "      loadScript(\"vega\")\n",
       "        .then(() => loadScript(\"vega-lite\"))\n",
       "        .then(() => loadScript(\"vega-embed\"))\n",
       "        .catch(showError)\n",
       "        .then(() => displayChart(vegaEmbed));\n",
       "    }\n",
       "  })({\"config\": {\"view\": {\"continuousWidth\": 400, \"continuousHeight\": 300}}, \"data\": {\"name\": \"data-12f38e144a92c41a85394e6a2e74e07f\"}, \"mark\": {\"type\": \"bar\", \"cornerRadiusTopLeft\": 3, \"cornerRadiusTopRight\": 3}, \"encoding\": {\"color\": {\"type\": \"nominal\", \"field\": \"repo_title\", \"title\": \"Key: repositories\"}, \"tooltip\": [{\"type\": \"nominal\", \"field\": \"repo\"}, {\"type\": \"nominal\", \"field\": \"year\"}, {\"type\": \"quantitative\", \"field\": \"count\"}], \"x\": {\"type\": \"nominal\", \"field\": \"year\", \"title\": \"Publication year\"}, \"y\": {\"type\": \"quantitative\", \"field\": \"count\", \"title\": \"Number of publications\"}}, \"height\": 600, \"width\": 800, \"$schema\": \"https://vega.github.io/schema/vega-lite/v4.8.1.json\", \"datasets\": {\"data-12f38e144a92c41a85394e6a2e74e07f\": [{\"repo\": \"oar\", \"year\": \"2020\", \"count\": 37.0, \"repo_title\": \"British Library Repository\"}, {\"repo\": \"oar\", \"year\": \"2019\", \"count\": 27.0, \"repo_title\": \"British Library Repository\"}, {\"repo\": \"oar\", \"year\": \"2018\", \"count\": 24.0, \"repo_title\": \"British Library Repository\"}, {\"repo\": \"oar\", \"year\": \"2017\", \"count\": 11.0, \"repo_title\": \"British Library Repository\"}, {\"repo\": \"oar\", \"year\": \"2016\", \"count\": 18.0, \"repo_title\": \"British Library Repository\"}, {\"repo\": \"oar\", \"year\": \"2015\", \"count\": 15.0, \"repo_title\": \"British Library Repository\"}, {\"repo\": \"oar\", \"year\": \"2014\", \"count\": 19.0, \"repo_title\": \"British Library Repository\"}, {\"repo\": \"oar\", \"year\": \"2013\", \"count\": 22.0, \"repo_title\": \"British Library Repository\"}, {\"repo\": \"oar\", \"year\": \"2012\", \"count\": 13.0, \"repo_title\": \"British Library Repository\"}, {\"repo\": \"oar\", \"year\": \"2011\", \"count\": 15.0, \"repo_title\": \"British Library Repository\"}, {\"repo\": \"oar\", \"year\": \"2010\", \"count\": 14.0, \"repo_title\": \"British Library Repository\"}, {\"repo\": \"ukwa\", \"year\": \"2013\", \"count\": 2.0, \"repo_title\": \"Web Archive Programme at BL\"}, {\"repo\": \"metadata\", \"year\": \"2020\", \"count\": 54.0, \"repo_title\": \"British Library Metadata Standards\"}, {\"repo\": \"metadata\", \"year\": \"2019\", \"count\": 66.0, \"repo_title\": \"British Library Metadata Standards\"}, {\"repo\": \"metadata\", \"year\": \"2018\", \"count\": 74.0, \"repo_title\": \"British Library Metadata Standards\"}, {\"repo\": \"metadata\", \"year\": \"2017\", \"count\": 69.0, \"repo_title\": \"British Library Metadata Standards\"}, {\"repo\": \"metadata\", \"year\": \"2016\", \"count\": 14.0, \"repo_title\": \"British Library Metadata Standards\"}, {\"repo\": \"eap\", \"year\": \"2019\", \"count\": 29.0, \"repo_title\": \"Endangered Archives Project\"}, {\"repo\": \"eap\", \"year\": \"2018\", \"count\": 19.0, \"repo_title\": \"Endangered Archives Project\"}, {\"repo\": \"eap\", \"year\": \"2017\", \"count\": 28.0, \"repo_title\": \"Endangered Archives Project\"}, {\"repo\": \"eap\", \"year\": \"2016\", \"count\": 34.0, \"repo_title\": \"Endangered Archives Project\"}, {\"repo\": \"eap\", \"year\": \"2015\", \"count\": 29.0, \"repo_title\": \"Endangered Archives Project\"}, {\"repo\": \"eap\", \"year\": \"2014\", \"count\": 28.0, \"repo_title\": \"Endangered Archives Project\"}, {\"repo\": \"eap\", \"year\": \"2013\", \"count\": 23.0, \"repo_title\": \"Endangered Archives Project\"}, {\"repo\": \"eap\", \"year\": \"2012\", \"count\": 26.0, \"repo_title\": \"Endangered Archives Project\"}, {\"repo\": \"eap\", \"year\": \"2011\", \"count\": 21.0, \"repo_title\": \"Endangered Archives Project\"}, {\"repo\": \"eap\", \"year\": \"2010\", \"count\": 23.0, \"repo_title\": \"Endangered Archives Project\"}, {\"repo\": \"eap\", \"year\": \"2009\", \"count\": 26.0, \"repo_title\": \"Endangered Archives Project\"}, {\"repo\": \"labs\", \"year\": \"2018\", \"count\": 16.0, \"repo_title\": \"British Library Labs\"}, {\"repo\": \"labs\", \"year\": \"2017\", \"count\": 40.0, \"repo_title\": \"British Library Labs\"}, {\"repo\": \"labs\", \"year\": \"2016\", \"count\": 48.0, \"repo_title\": \"British Library Labs\"}, {\"repo\": \"labs\", \"year\": \"2013\", \"count\": 1.0, \"repo_title\": \"British Library Labs\"}]}}, {\"mode\": \"vega-lite\"});\n",
       "</script>"
      ],
      "text/plain": [
       "alt.Chart(...)"
      ]
     },
     "execution_count": 10,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "import altair as alt\n",
    "\n",
    "alt.Chart(df).mark_bar(\n",
    "    cornerRadiusTopLeft=3,\n",
    "    cornerRadiusTopRight=3,\n",
    ").encode(\n",
    "    x=alt.X('year', title='Publication year'),\n",
    "    y=alt.Y('count', title='Number of publications'),\n",
    "    color=alt.Color('repo_title', title='Key: repositories'),\n",
    "    tooltip=['repo', 'year', 'count'],\n",
    ").properties(\n",
    "    width=800,\n",
    "    height=600,\n",
    ")"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
